﻿declare function reInitializationMce(): any;
var isChangedWidget: boolean = false;
var attributeData: string = "";
class ContentWidget extends ZnodeBase {

    constructor() {
        super();
    }

    GetVariants() {
        var contentWidgetId = $("#ContentWidgetId").val()
        ZnodeBase.prototype.BrowseAsidePoupPanel('/ContentWidget/GetVariants?contentWidgetId=' + contentWidgetId, 'VariantPanel');
    }

    GetUnassociatedProfiles(): any {
        var widgetKey = $("#WidgetKey").val()
        var localeId = $('#ddlLocale option:selected').val();
        Endpoint.prototype.GetUnassociatedProfileList(widgetKey, localeId, function (response) {
            var profileList = response.ProfileList;
            $('#profileList').empty()
            $.each(profileList, function (index, element) {
                $('#profileList').append($(`<option value="${element.Value}"><text>${element.Text}</text></option>`));
            });
        });
    }

    BindVariantDropdown(data): any {
        if (data != undefined && data != null) {
            var variantList = data.VariantList;
            $('#ddlWidgetVariants').empty()
            $('#ddlWidgetVariants').removeAttr("disabled");
            $.each(variantList, function (index, element) {
                element.WidgetTemplateId = (element.WidgetTemplateId == null) ? "" : element.WidgetTemplateId  
                $('#ddlWidgetVariants').append($(`<option value="${element.WidgetProfileVariantId}" data-template = "${element.WidgetTemplateId}" ><text>Variant For ${element.ProfileName} And ${element.LocaleName}</text></option>`));
            });
            ZnodeBase.prototype.HideLoader();
            ContentWidget.prototype.GetAssociatedAttributes();         
        }
        ZnodeBase.prototype.CancelUpload('VariantPanel');
    }

    BindAssociatedAttributeValue(data): any {
        if (data != undefined && data != null) {
            $("#AssociatedGroups").html("");
            $("#AssociatedGroups").html(data);
            $.getScript("/Scripts/References/DynamicValidation.js");
            reInitializationMce();
            $("#DynamicHeading").text("Edit - " + $('#ddlWidgetVariants option:selected').text())
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessChangesSaved"), "success", isFadeOut, fadeOutTime);
 
        }
    }

    GetAssociatedAttributes(): any {
        var entityType = "Widget";
        var entityId = $('#ddlWidgetVariants option:selected').val();
        $('#drpwidgetTemplate').val($('#ddlWidgetVariants option:selected').attr("data-template"))
        $("#EntityId").val($('#ddlWidgetVariants option:selected').val());
        $("#EntityType").val("Widget")
        if (entityId > 0) {
            Endpoint.prototype.GetAssociatedVariants(entityId, entityType, function (response) {   
                $("#AssociatedGroups").html("");
                $("#AssociatedGroups").html(response);
                 reInitializationMce();
                $("#DynamicHeading").text("Edit - " + $('#ddlWidgetVariants option:selected').text())
                attributeData = $("#frmGlobalAttribute").serialize();
                $("#variantWidgetTemplate").show();
                $("#variantAttributeData").show();
                ZnodeBase.prototype.HideLoader();
            });
        }
        ZnodeBase.prototype.HideLoader();
    }

    DeleteContentWidget(control): any {
        var contentWidgetId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (contentWidgetId.length > 0) {
            Endpoint.prototype.DeleteContentWidget(contentWidgetId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    DeleteVariant(): any {        
        var variantId = $('#ddlWidgetVariants option:selected').val();
        var widgetKey = $('#WidgetKey').val()
        if (variantId > 0) {
            Endpoint.prototype.DeleteAssociatedVariant(variantId, widgetKey, function (response) {
                if (response.status) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
                    $("#ddlWidgetVariants option[value=" + variantId + "]").remove();
                    if ($("#ddlWidgetVariants option").length > 0) {
                        ContentWidget.prototype.GetAssociatedAttributes();
                    }
                    else {
                        $("#AssociatedGroups").html("");
                    }
                }
                else {
                    ZnodeBase.prototype.HideLoader();
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
                }     
            });
        }
    }



    fnShowHide(control, divId: string) {
        control = $(divId).parent("div").find(".panel-heading").find("h4");
        if ($(control).hasClass("collapsed")) {
            $(control).removeClass("collapsed");
        }
        else {
            $(control).addClass("collapsed");
        }
        $("#" + divId).children(".panel-body").children(".widgetAtribute").show();
        $("#" + divId).slideToggle();
    }

    SaveEntityAttribute(backURL: string) {
        if ($('#drpwidgetTemplate').val() == "") {
            $("#errorRequiredTemplate").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorWidgetTemplate")).addClass("field-validation-error").show();
            $("#drpwidgetTemplate").addClass('input-validation-error');
            ContentWidget.prototype.ValidateForm();
        }
        else {
            if (isChangedWidget && ((attributeData) != $("#frmGlobalAttribute").serialize())) {
                var variantId = $('#ddlWidgetVariants option:selected').val();
                var widgetTemplateId = $('#drpwidgetTemplate').val();
                Endpoint.prototype.AssociateWidgetTemplate(variantId, widgetTemplateId, function (response) {
                    $('#ddlWidgetVariants option:selected').attr("data-template", $('#drpwidgetTemplate').val())
                    isChangedWidget = false;
                })

                $("#frmGlobalAttribute").submit();
                ContentWidget.prototype.ValidateForm();
                ZnodeBase.prototype.HideLoader();
            }
            else if (isChangedWidget && ((attributeData) == $("#frmGlobalAttribute").serialize())) {
                var variantId = $('#ddlWidgetVariants option:selected').val();
                var widgetTemplateId = $('#drpwidgetTemplate').val();
                Endpoint.prototype.AssociateWidgetTemplate(variantId, widgetTemplateId, function (response) {
                    $('#ddlWidgetVariants option:selected').attr("data-template", $('#drpwidgetTemplate').val())
                    isChangedWidget = false;
                })
                ZnodeBase.prototype.HideLoader();
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessChangesSaved"), "success", isFadeOut, fadeOutTime);
            }
            else if (!isChangedWidget && ((attributeData) != $("#frmGlobalAttribute").serialize())) {
                if (!$("#frmGlobalAttribute").valid()) {
                    $("#frmGlobalAttribute .input-validation-error").closest('.panel-collapse').attr("style", "display:block");
                }
                $("#frmGlobalAttribute").submit();
                ContentWidget.prototype.ValidateForm();
            }

            else {
                $("#frmGlobalAttribute").submit();
                ContentWidget.prototype.ValidateForm();
            }

        }
    }

    ValidateForm(): any {
        if (!$("#frmGlobalAttribute").valid()) {
            $("#frmGlobalAttribute .input-validation-error").closest('.panel-collapse').attr("style", "display:block");
        }
    }

    OnTemplateChange(): any {
        if ($('#drpwidgetTemplate').val() != "") {
            $("#drpwidgetTemplate").removeClass("input-validation-error");
            $("#errorRequiredTemplate").text('').text("").removeClass("field-validation-error").hide();
        }     
        isChangedWidget = true
    }

    SetActiveGroup(group: string) {
                $(this).addClass('active-tab-validation');
    }

    ValidateData(): boolean {
        var isValid = true;
        if ($("#txtPortalName").is(':visible') && ($(".portalsuggestion *").attr('readonly') == undefined)) {
            if ($("#txtPortalName").val() == "") {
                $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectPortal")).addClass("field-validation-error").show();
                $("#txtPortalName").parent("div").addClass('input-validation-error');
                isValid = false;

            }
        }
        if ($('#ContentWidgetId').val() < 1 && $('#WidgetKey').val() !='') {
                Endpoint.prototype.IsWidgetExist($('#WidgetKey').val(), function (response) {
                    if (response.data) {
                        $("#WidgetKey").addClass("input-validation-error");
                        $("#errorSpanWidgetKey").addClass("error-msg");
                        $("#errorSpanWidgetKey").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistAttributeWidgetKey"));
                        $("#errorSpanWidgetKey").show();
                        isValid = false;
                       
                    }
                });
            }        
        return isValid;
    }

    public IsGlobal() {
        if ($('#PortalId').val() == "0" && window.location.href.toLowerCase().indexOf("update") !== -1) {
            $("#IsGlobalContentWidget").attr("checked", "checked");
        }
        if ($("#IsGlobalContentWidget").is(":checked")) {
            $("#txtPortalName").val('');
            $("#IsGlobalContentWidget").val("true")
            $('#hdnPortalId').val(0);
            $(".portalsuggestion *").attr('readonly', true);
            $('.portalsuggestion').css({ pointerEvents: "none" })
            $(".fstElement").css({ "background-color": "#e7e7e7" });
            $(".fstElement").removeClass('input-validation-error');
            $("#errorRequiredStore").text('').text("").removeClass("field-validation-error").hide();
            $("#txtPortalName").removeClass('input-validation-error');
            $(".fstToggleBtn").html("Select Store");
            $(".fstResultItem").removeClass('fstSelected');
            $('#PortalId').val("0");
            $('#hdnPortalId').val("0");
            $('.fstToggleBtn').html("All Stores");
            $('#StoreName').val("All Stores");
        }
        else {
            $(".portalsuggestion *").attr('readonly', false);
            $('.portalsuggestion').css({ pointerEvents: "visible" })
            $("#IsGlobalContentWidget").val("false")
            $(".fstElement").css({ "background-color": "#fff" });
            $('.fstToggleBtn').html("Select Store");
        }
    }

    public DeleteWidgetVariant(): void {
        $("#DeleteWidgetVariant").modal("show");
    }

    OnSelectPortalResult(item: any): any {
        if (item != undefined) {
            let portalName: string = item.text;
            $('#StoreName').val(portalName);
            Store.prototype.OnSelectStoreAutocompleteDataBind(item);
        }
    }

    SubmitWidgetVariantsForm(e): any {
        e.preventDefault();
        if (!($("#profileList").val())) {
            $("#profileListError").removeClass('hidden');
        }
        else {
            $("#profileListError").addClass('hidden');
            $("#formWidgetVariants").submit();
        }
    }
  
}