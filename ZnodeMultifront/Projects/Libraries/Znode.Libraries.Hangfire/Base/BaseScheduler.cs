﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Libraries.Hangfire
{
    public class BaseScheduler
    {
        public int UnAuthorizedErrorCode = 32;
        public string TokenInvalidErrorMessage = "Token is expired";
        public int requesttimeout = 600000;//10 min

        #region Public Methods
        //Logs all the messages in the file daywise
        public void LogMessage(string Message, string schedulerName) => WriteLogs(Message, schedulerName);

        public bool CheckTokenIsInvalid(WebException ex)
        {
            bool isTokenInValid = false;
            if (ex?.Response != null)
            {
                using (WebResponse response = ex.Response)
                {
                    using (Stream data = response?.GetResponseStream())
                        if (data != null)
                        {
                            using (var reader = new StreamReader(data))
                            {
                                BaseResponse objBaseResponse = new BaseResponse();
                                objBaseResponse = JsonConvert.DeserializeObject<BaseResponse>(reader?.ReadToEnd());
                                if (objBaseResponse?.ErrorCode == UnAuthorizedErrorCode && objBaseResponse?.ErrorMessage == TokenInvalidErrorMessage)
                                {
                                    return isTokenInValid = true;
                                }
                            }
                        }
                }
            }
            return isTokenInValid;
        }

        public string GetToken(string ApiUrl, string AuthorizationHeader)
        {
            string token = string.Empty;
            StringResponse result = new StringResponse();
            string endpoint = $"{ApiUrl}/token/genaratetoken";

            //Get response.
            ApiStatus status = new ApiStatus();
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false;
            req.Method = "GET";
            req.Timeout = requesttimeout;
            req.Headers.Add($"Authorization: Basic { AuthorizationHeader }");
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
                {
                    Stream datastream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(datastream);
                    string jsonString = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<StringResponse>(jsonString);
                    token = result?.Response;
                    reader.Close();
                    datastream.Close();
                }
            }
            catch (Exception ex)
            {
                LogMessage($"{DateTime.Now}-- Failed to get token: {ex.Message}", ApiUrl);
            }
            return token;
        }
        #endregion

        #region Private Methods

        //This method will write the messages to the log file.
        private void WriteLogs(string message, string schedulerName)
        {
            StringBuilder errMsg = new StringBuilder();
            errMsg.AppendLine();
            errMsg.AppendLine("************* " + DateTime.Now.ToString() + " *************");
            errMsg.AppendLine(message);
            errMsg.AppendLine("***********************************************");

            WriteTextStorage(errMsg.ToString(), schedulerName);
        }

        //Writes text file to persistant storage.
        private void WriteTextStorage(string fileData, string schedulerName)
        {
            try
            {
                string path = CheckCreateLogDirectory(schedulerName);
                File.AppendAllText(path, fileData);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // Check if directory exists if not create the directory and file
        private string CheckCreateLogDirectory(string schedulerName)
        {
            string logFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}\\Logs\\";

            FileInfo fileInfo = new FileInfo(logFilePath);
            if (!fileInfo.Directory.Exists)
                fileInfo.Directory.Create();

            logFilePath = $"{logFilePath}\\{schedulerName}\\";
            FileInfo fileInformation = new FileInfo(logFilePath);
            if (!fileInformation.Directory.Exists)
                fileInformation.Directory.Create();

            string fileNameWithPath = $"{logFilePath}\\ZnodeSchedulerLog_{DateTime.Now.ToString("yyyy-MM-dd")}.txt";
            if (!File.Exists(fileNameWithPath))
                File.Create(fileNameWithPath).Dispose();

            fileInfo = null;
            fileInformation = null;
            return fileNameWithPath;
        }
        #endregion
    }
}
